#!/usr/bin/env nextflow

process netmhcstabpan {
// Predicts binding stability of peptides to any known MHC molecule using
// artificial neural networks (ANNs)
//
// input:
// output:

// require:
//   PEPTIDES_AND_ALLELES

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'netmhcstabpan_container'
  label 'netmhcstabpan'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/netmhcstabpan"
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(peptides), path(alleles)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*fa.netmhcstabpan.txt"), emit: c1_stabs

  script:
  """
  export TMPDIR="\${PWD}"
  /netMHCstabpan-1.0/bin/netMHCstabpan -listMHC > allowed_alleles

  for i in `cat *netmhcpan.alleles* | sed 's/,/\\n/g'`; do echo \${i}; grep "^\${i}\$" allowed_alleles | cat >> final_alleles; done

  EXPLICIT_ALLELES=\$(cat final_alleles | sed -z 's/\\n/\\t/g ' | sed 's/\\t\$//g')
  PEPTIDES_FILENAME="${peptides}"

  grep -v "^;" ${peptides} > peptides.no_comms.fa

  if [[ -s ${peptides} ]]; then
    export TMPDIR="\${PWD}"
    for allele in \${EXPLICIT_ALLELES}; do
      /netMHCstabpan-1.0/bin/netMHCstabpan -l 8,9,10,11 -a \${allele} -inptype 0 -f peptides.no_comms.fa > \${PEPTIDES_FILENAME%.aa.fa}.\${allele}.netmhcstabpan.txt&
    done
  else
    PEPTIDES_FILENAME="${peptides}"
    echo "NO PEPTIDES" > \${PEPTIDES_FILENAME%.aa.fa}.netmhcstabpan.txt
  fi
  wait

  for allele in \${EXPLICIT_ALLELES}; do
    cat \${PEPTIDES_FILENAME%.aa.fa}.\${allele}.netmhcstabpan.txt >> \${PEPTIDES_FILENAME%.aa.fa}.netmhcstabpan.txt
  done 
  """
}


process netmhcstabpan_rna {
// Predicts binding stability of peptides to any known MHC molecule using
// artificial neural networks (ANNs)
//
// input:
// output:

// require:
//   PEPTIDES_AND_ALLELES

  tag "${dataset}/${pat_name}/${run}"
  label 'netmhcstabpan_container'
  label 'netmhcstabpan'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/netmhcstabpan"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(peptides), path(alleles)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*fa.netmhcstabpan.txt"), emit: c1_stabs

  script:
  """
  export TMPDIR="\${PWD}"
  /netMHCstabpan-1.0/bin/netMHCstabpan -listMHC > allowed_alleles

  for i in `cat *netmhcpan.alleles* | sed 's/,/\\n/g'`; do echo \${i}; grep "^\${i}\$" allowed_alleles | cat >> final_alleles; done

  EXPLICIT_ALLELES=\$(cat final_alleles | sed -z 's/\\n/\\t/g ' | sed 's/\\t\$//g')
  PEPTIDES_FILENAME="${peptides}"

  grep -v "^;" ${peptides} > peptides.no_comms.fa #Apparent netmhcpan can see commented lines.

  if [[ -s ${peptides} ]]; then
    export TMPDIR="\${PWD}"
    for allele in \${EXPLICIT_ALLELES}; do
      /netMHCstabpan-1.0/bin/netMHCstabpan -l 8,9,10,11 -a \${allele} -inptype 0 -f $peptides.no_comms.fa > \${PEPTIDES_FILENAME%.aa.fa}.\${allele}.netmhcstabpan.txt&
    done
  else
    PEPTIDES_FILENAME="${peptides}"
    echo "NO PEPTIDES" > \${PEPTIDES_FILENAME%.aa.fa}.netmhcstabpan.txt
  fi
  
  for allele in \${EXPLICIT_ALLELES}; do
    cat \${PEPTIDES_FILENAME%.aa.fa}.\${allele}.netmhcstabpan.txt >> \${PEPTIDES_FILENAME%.aa.fa}.netmhcstabpan.txt
  done 
  """
}
